import 'package:flutter_document/pages/index.dart';
import 'package:flutter_document/pages/home/index.dart';
import 'package:get/get.dart';

List<GetPage<dynamic>> pages = [
  GetPage(name: '/', page: () => Toolbar()),
  GetPage(name: '/menu', page: () => Toolbar()),
];