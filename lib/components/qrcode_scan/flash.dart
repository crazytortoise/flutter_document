import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class FlashToggle extends StatefulWidget {
  final QRViewController? controller;

  const FlashToggle({Key? key, this.controller}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FlashToggleState();
}

class _FlashToggleState extends State<FlashToggle> {

  bool isFlashOn = false;

  @override
  Widget build(BuildContext context) {

    // 状态栏高度
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return Positioned(
      top: statusBarHeight + 15,
      right: 10,
      child: FloatingActionButton(
        child: Icon(isFlashOn ? Icons.flash_on : Icons.flash_off),
        onPressed: () async {
          setState(() {
            isFlashOn = !isFlashOn;
            widget.controller?.toggleFlash();
          });
        },
      ),
    );
  }
}
