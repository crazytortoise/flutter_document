import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Album extends StatefulWidget {
  final Function(String)? callback;
  final IconData? iconName;
  final Color? color;
  final double? iconSize;

  const Album({
    Key? key,
    this.callback,
    this.iconName = Icons.photo_size_select_actual_outlined,
    this.iconSize = 30,
    this.color = Colors.white,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AlbumState();
}

class _AlbumState extends State<Album> {

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(widget.iconName),
        color: widget.color,
        iconSize: widget.iconSize,
        onPressed: () => _scanBarcode()
    );
  }

  Future<void> _scanBarcode() async {

    final XFile? image = await ImagePicker().pickImage(source: ImageSource.gallery);

    if (image != null) {
      print(image.path + '=============image');

      widget.callback!(image.path);
    }
  }
}
