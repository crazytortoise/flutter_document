import 'package:flutter/material.dart';

import 'package:flutter_document/components/f_row/f_row.dart';

class LevelList extends StatefulWidget {
  LevelList({Key? key}) : super(key: key);

  _LevelListState createState() => _LevelListState();
}

class _LevelListState extends State<LevelList> {

  List<Map> data = [
    {
      'title': '第一集',
      'desc': 'Type 1 Type 1 Type 1 Type 1 Type 1 Type 1',
    },
    {
      'title': '第二集',
      'desc': 'Type 2 Type 2 Type 2 Type 2 Type 2 Type 2',
    },
    {
      'title': '第三集',
      'desc': 'Type 3 Type 3 Type 3 Type 3 Type 3 Type 3',
    },
    {
      'title': '第四集',
      'desc': 'Type 4 Type 4 Type 4 Type 4 Type 4 Type 4',
    },
    {
      'title': '第五集',
      'desc': 'Type 5 Type 5 Type 5 Type 5 Type 5 Type 5',
    },
    {
      'title': '第六集',
      'desc': 'Type 6 Type 6 Type 6 Type 6 Type 6 Type 6',
    },
    {
      'title': '第七集',
      'desc': 'Type 6 Type 6 Type 6 Type 6 Type 6 Type 6',
    },
    {
      'title': '第八集',
      'desc': 'Type 6 Type 6 Type 6 Type 6 Type 6 Type 6',
    },
    {
      'title': '第九集',
      'desc': 'Type 6 Type 6 Type 6 Type 6 Type 6 Type 6',
    },
    {
      'title': '第十集',
      'desc': 'Type 6 Type 6 Type 6 Type 6 Type 6 Type 6',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: ListView.builder(
          padding: const EdgeInsets.all(0),
          itemCount: data.length,
          itemBuilder: (context, index) {
            String title = data[index]['title'] ?? '';
            String desc = data[index]['desc'] ?? '';

            return FRow(
              border: Border(
                  bottom: BorderSide(
                      width: index == data.length - 1 ? 0 : 1,
                      color: const Color.fromRGBO(242, 242, 242, 1))),
              height: 70,
              padding: const EdgeInsets.only(left: 20, top: 0, right: 20, bottom: 0),
              leftChild: leftChild(title: title, desc: desc),
              rightChild: rightChild(
                icon: const Icon(Icons.tv_outlined,
                    size: 20, color: Color.fromRGBO(104, 68, 60, 1)),
                bgColor: Colors.white,
              ),
              onPressed: () => {print(title)},
            );
          }),
    );
  }

  Widget rightChild(
      {required Icon icon, required Color bgColor, bool showBorder = true}) {
    return Container(
      width: 40,
      height: 40,
      decoration: BoxDecoration(
        color: bgColor,
        border: Border.all(
          color:
          showBorder ? Color.fromRGBO(99, 71, 58, 1) : Colors.transparent,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 2),
        child: icon,
      ),
    );
  }

  Column leftChild(
      {required String title, required String desc, bool titleBlod = true}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                  color: Color.fromRGBO(56, 56, 56, 1),
                  fontSize: 16,
                  fontWeight: titleBlod ? FontWeight.bold : FontWeight.normal),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Text(
              desc,
              style: const TextStyle(
                  color: Color.fromRGBO(128, 128, 128, 1), fontSize: 12),
            )
          ],
        )
      ],
    );
  }

}