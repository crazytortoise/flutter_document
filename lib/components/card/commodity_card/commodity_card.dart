import 'package:flutter/material.dart';
import 'package:flutter_document/components/card/common/commodity_desc.dart';
import 'package:flutter_document/components/card/common/commodity_name.dart';
import 'package:flutter_document/components/card/common/commodity_num.dart';
import 'package:flutter_document/components/card/common/commodity_price.dart';

class CommodityCard extends StatefulWidget {
  final String commodityName;
  final String commodityDesc;
  final String price;
  final int num;

  const CommodityCard(
      this.commodityName,
      this.commodityDesc,
      this.price,
      this.num,
      {super.key}
      );

  @override
  _CommodityCard createState() => _CommodityCard();
}

class _CommodityCard extends State<CommodityCard> {

  @override
  Widget build(BuildContext context) {

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 2.0,
      color: Colors.white,
      child: Container(
        width: MediaQuery.of(context).size.width / 2 - 30,
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              'lib/assets/images/card/card.png',
              width: double.infinity,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween, // 两边分布
              crossAxisAlignment: CrossAxisAlignment.start, // 纵向靠上
              children: [
                SizedBox(
                  width: 200,
                  height: 70,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start, // 纵向靠上
                    children: [
                      CommodityName(widget.commodityName), // 商品名称
                      CommodityDesc(widget.commodityDesc), // 商品描述
                    ],
                  ),
                ),
                SizedBox(
                  width: 200,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween, // 两边分布
                    crossAxisAlignment: CrossAxisAlignment.center, // 横向居中
                    children: [
                      CommodityPrice(widget.price), // 商品价格
                      CommodityNum(widget.num), // 商品数量
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

}