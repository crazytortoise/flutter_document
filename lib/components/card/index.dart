import 'package:flutter/material.dart';
import 'package:flutter_document/components/card/commodity_card/commodity_card.dart';
import 'package:flutter_document/components/card/default_card/default_card.dart';

class RCard extends StatefulWidget {
  const RCard({super.key});

  @override
  _RCard createState() => _RCard();
}

class _RCard extends State<RCard> {

  @override
  Widget build(BuildContext context) {
    String commodityName = 'Ipad';
    String commodityDesc = '商品描述商品描述商品描述商品描述商品描述商品描述商品描述';
    String price = '120.0';
    int num = 0;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        DefaultCard(commodityName, commodityDesc, price, num),
        CommodityCard(commodityName, commodityDesc, price, num),
      ],
    );
  }

}