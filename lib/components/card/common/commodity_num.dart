import 'package:flutter/material.dart';

class CommodityNum extends StatefulWidget {
  final int num;

  CommodityNum(this.num, {super.key});

  _CommodityNum createState() => _CommodityNum();

}

class _CommodityNum extends State<CommodityNum> {
  @override
  Widget build(BuildContext context) {
    return widget.num != 0 ? Text(
      'x${ widget.num }',
    ) : const Text('');
  }
}