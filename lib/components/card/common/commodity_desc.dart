import 'package:flutter/material.dart';

class CommodityDesc extends StatefulWidget {
  final String commodityDesc;

  CommodityDesc(this.commodityDesc, {super.key});

  _CommodityDesc createState() => _CommodityDesc();

}

class _CommodityDesc extends State<CommodityDesc> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.commodityDesc,
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      style: const TextStyle(
        fontSize: 14,
        fontStyle: FontStyle.normal,
        color: Colors.black45,
      ),
    );
  }
}