import 'package:flutter/material.dart';

class CommodityName extends StatefulWidget {
  final String commodityName;

  CommodityName(this.commodityName, {super.key});

  _CommodityName createState() => _CommodityName();

}

class _CommodityName extends State<CommodityName> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.commodityName,
      textAlign: TextAlign.left,
      style: const TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
        color: Colors.blue,
      ),
    );
  }
}