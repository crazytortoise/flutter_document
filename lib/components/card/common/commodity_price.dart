import 'package:flutter/material.dart';

class CommodityPrice extends StatefulWidget {
  final String price;

  CommodityPrice(this.price, {super.key});

  _CommodityPrice createState() => _CommodityPrice();

}

class _CommodityPrice extends State<CommodityPrice> {
  @override
  Widget build(BuildContext context) {
    return Text(
      '¥ ' + widget.price,
      style: const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
        color: Colors.red,
      ),
    );
  }
}