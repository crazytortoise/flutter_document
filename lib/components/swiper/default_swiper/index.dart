import 'package:flutter/material.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter_document/components/swiper/common/photo_view.dart';

class DefaultSwiper extends StatelessWidget {
  final List<String> images;
  final int index;
  final double? height;
  final Function? onTap;

  /// 轮播图
  /// ```
  /// @param {List<String>} images - 轮播图地址
  /// @param {int} index - 初始下标位置
  /// @param {double} height - 容器高度
  /// @param {Function} onTap - 点击事件回掉
  /// ```
  DefaultSwiper(this.images, {
    super.key,
    this.index = 0,
    this.height = 288.0,
    this.onTap = _defaultFunction,
  });

  static void _defaultFunction(int index) {
    // 默认的空函数
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SizedBox(
      width: width,
      height: height,
      child: Swiper(
        index: index,
        itemBuilder: (BuildContext context,int index){
          return Image.asset(images[index], fit: BoxFit.fitHeight);
        },
        itemCount: images.length,
        pagination: const SwiperPagination(
            builder:  DotSwiperPaginationBuilder(
                size: 8,
                activeSize: 8
            )
        ),
        autoplay: true,
        duration: 500,
        autoplayDelay: 5000,
        onTap: (index) => {
          showDialog(
            context: context,
            useSafeArea: false,
            builder: (BuildContext context) {
              return PhotoViewer(imageUrls: images, initIndex: index);
            },
          ),

        },
      ),
    );
  }
}