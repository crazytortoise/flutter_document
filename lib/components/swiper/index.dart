import 'package:flutter/material.dart';
import 'package:flutter_document/components/swiper/column_swiper/index.dart';
import 'package:flutter_document/components/swiper/default_swiper/index.dart';

class RSwiper extends StatefulWidget {
  const RSwiper({super.key});

  @override
  _RSwiper createState() => _RSwiper();
}

class _RSwiper extends State<RSwiper> {
  @override
  Widget build(BuildContext context) {

    print("MediaQuery.of(context).size.width ${ MediaQuery.of(context).size.width }");

    // 页面内容
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        DefaultSwiper(
          [
            'lib/assets/images/home/1.jpeg',
            'lib/assets/images/home/1.png',
            'lib/assets/images/home/2.jpeg',
            'lib/assets/images/home/3.jpeg',
            'lib/assets/images/home/4.jpeg',
            'lib/assets/images/home/5.jpeg',
          ],
        ),
        Text(''),
        ColumnSwiper(
          [
            'lib/assets/images/home/1.jpeg',
            'lib/assets/images/home/1.png',
            'lib/assets/images/home/2.jpeg',
            'lib/assets/images/home/3.jpeg',
            'lib/assets/images/home/4.jpeg',
            'lib/assets/images/home/5.jpeg',
          ],
        ),
      ],
    );
  }
}
