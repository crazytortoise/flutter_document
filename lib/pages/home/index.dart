import 'package:flutter/material.dart';
import 'package:flutter_document/components/qrcode_scan/qrcode.dart';
import 'package:flutter_document/components/levelList/level_list.dart';
import 'package:flutter_document/components/swiper/default_swiper/index.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(246, 246, 246, 0.8),
      body: Column(
        children: <Widget>[
          // 头部banner
          Stack(
            children: <Widget>[
              // 轮播图
              SizedBox(
                width: double.infinity,
                height: 280,
                child: DefaultSwiper(
                  [
                    'lib/assets/images/home/1.jpeg',
                    'lib/assets/images/home/1.png',
                    'lib/assets/images/home/2.jpeg',
                    'lib/assets/images/home/3.jpeg',
                    'lib/assets/images/home/4.jpeg',
                    'lib/assets/images/home/5.jpeg',
                  ],
                  height: double.infinity,
                ),
              ),
              // 扫描二维码
              Positioned(
                top: 50,
                right: 10,
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(0, 0, 0, 0.30196078431372547),
                    border: Border.all(
                      color: false
                          ? const Color.fromRGBO(99, 71, 58, 1)
                          : Colors.transparent,
                    ),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const QRCode(),
                      ));
                    },
                    child: const Icon(
                      Icons.qr_code_scanner,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          // 广告
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10),
            height: 80,
            width: double.infinity,
            child: Image.asset(
              'lib/assets/images/home/1.png',
              fit: BoxFit.fitWidth,
            ),
          ),
          // 健身推荐
          LevelList(),
        ],
      ),
    );
  }


}
