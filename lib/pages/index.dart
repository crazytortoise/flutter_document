import 'package:flutter/material.dart';
import 'package:flutter_document/pages/home/index.dart';
import 'package:flutter_document/pages/menu/index.dart';

class Toolbar extends StatefulWidget {
  Toolbar({Key? key}) : super(key: key);

  @override
  State<Toolbar> createState() => _ToolbarState();
}

class _ToolbarState extends State<Toolbar> {
  int cuttentIndex = 0;

  PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Home(),
          Menu(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Color.fromRGBO(38, 127, 255, 1.0),
        unselectedItemColor: Color.fromRGBO(43, 76, 126, .5),
        currentIndex: cuttentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          print(index);
          setState(() {
            _pageController.jumpToPage(index);
            cuttentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '首页',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu_book),
            label: '菜单',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.article_rounded),
            label: '订单',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_cart,
            ),
            label: '购物车',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '我的',
          ),
        ],
      ),
    );
  }
}
