import 'package:flutter_document/components/card/index.dart';
import 'package:flutter_document/components/swiper/index.dart';

List<Map<String, dynamic>> pages = [
  {
    'pageName': 'Swiper',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '卡片',
    'pageComponent': RCard(),
  },
  {
    'pageName': '按钮3',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮4',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮5',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮6',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮7',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮8',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮9',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮10',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮11',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮12',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮13',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮14',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮15',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮16',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮17',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮18',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮19',
    'pageComponent': RSwiper(),
  },
  {
    'pageName': '按钮20',
    'pageComponent': RSwiper(),
  }
];
