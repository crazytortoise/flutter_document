import 'package:flutter/material.dart';
import 'package:flutter_document/pages/menu/common/pages.dart';

class Menu extends StatefulWidget {
  const Menu({super.key});

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  int page_index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('左侧选择列表'),
        ),
        body: Row(
          children: [
            Expanded(
              flex: 2,
              child: ListView.builder(
                padding: EdgeInsets.only(top: 2),
                itemCount: pages.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        page_index = index;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: page_index == index ? Color.fromARGB(30, 59, 152, 255) : Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: page_index == index ? Colors.lightBlue : Colors.white,
                            width: 3.0,
                          ),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              pages[index]['pageName'],
                              style: TextStyle(
                                color: Colors.lightBlue,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            Expanded(
              flex: 8,
              child: pages[page_index]['pageComponent'],
            ),
          ],
        ));
  }
}
