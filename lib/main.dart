import 'package:flutter/material.dart';
import 'package:flutter_document/route/get_pages.dart';
import 'package:get/route_manager.dart';

void main() {
  runApp(GetMaterialApp(
    title: 'Test Flutter',
    initialRoute: '/',
    getPages: pages,
  ));
}
